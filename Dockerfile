FROM node:20-alpine
RUN apk add --no-cache git
RUN git clone https://gitlab.com/mother3021315/Child-Development.git
WORKDIR /Child-Development
RUN npm install
CMD npm start